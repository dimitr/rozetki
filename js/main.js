/*MenuButtonAnimate*/
    function menuBtn(){
        $('.menu_btn').on('click', function(e){
            $(this).toggleClass('open');
            $('.menu_list').slideToggle(350);
            $('.menu').toggleClass('open');
            $('.menu_list').toggleClass('open');
            $('.fon_menu').fadeToggle(200);
            //$('body').toggleClass('hiddens');
            $('.lang_panel').slideToggle(50);

            if($(this).hasClass('open')){
                $('.fonmenu').animate({'width':'350px'},300);
                $('.totop').fadeOut(100);
                $('.fonmenu').addClass('opens');
                $('body').addClass('open_menu');
                $('.menu_bl').delay(400).fadeIn(300);
                $('.menu_bl').delay(400).addClass('open');
                $('.fonmenu #page-background').fadeIn(100);
            } else {                
                $('.menu_bl').fadeOut(300);
                $('.fonmenu').delay(400).animate({'width':'0px'},300);
                $('.menu_bl').delay(400).removeClass('open');
                $('.totop').delay(600).fadeIn(200);
                $('.fonmenu').removeClass('opens');
                $('body').removeClass('open_menu');
                $('.main_menu li:eq(0)').find('ul').slideUp(300);
                $('.main_menu li:eq(0)').removeClass('open');
                 $('.fonmenu #page-background').delay(600).fadeOut(100);
            }

            e.preventDefault(0);
        });

    }
/*end*/

/*MobileMenu*/
    function MobileMenu(){
        var main_menu = $('.main_menu'),
            parent_a = $('.main_menu .parent_a'),
            window_w = $(document).width();

            if(window_w<=962){
                parent_a.on('click', function(e){
                    if($(this).hasClass('open')){
                        $(this).removeClass('open');
                        $(this).parent().removeClass('mb-open').find('.submenu').slideUp(200);
                        $(this).parent().removeClass('mb-open');
                    } else {
                        parent_a.removeClass('open');
                        $('.main_menu .submenu').slideUp(200);
                        main_menu.find('li').removeClass('mb-open');
                        $(this).addClass('open');
                        $(this).parent().addClass('mb-open').find('.submenu').slideDown(200);
                        $(this).parent().addClass('mb-open');
                    }

                    e.preventDefault();
                });
            }
    }
/**/

function InputPlaceholder(){
    var intext = $('.in-text'),
        placeholder = $('.inputblock .placeholder');

        if(intext.length){
            intext.focus(function() {
                $(this).parent().addClass('focus');
            });

            placeholder.on('click',function() {
                $(this).parent().addClass('focus');
                 $(this).parent().find('.in-text').focus();
            });

            $(intext).focusout(function(){
                if($(this).val() == ''){
                    $(this).addClass('empty');
                    $(this).parent().removeClass('focus');
                } else {
                    $(this).removeClass('empty');
                }
            }); 
            

        }
}



/*ScrollAnimate*/
    function AnimateScroll(event){
        var scrollPos = $(document).scrollTop() + 650;
        var scrollPos2 = $(document).scrollTop() + 300;
        var scrollPos3 = $(document).scrollTop() + 800;
        var scrollPos4 = $(document).scrollTop() + 650;


        $('.image-container').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos && currLink.offset().top + currLink.height() > scrollPos) {                
                currLink.addClass('is-visible');
            }  

            if (currLink.offset().top < scrollPos){
                currLink.addClass('is-visible');
            }          
        });

        $('.for_animate').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos && currLink.offset().top + currLink.height() > scrollPos) {                
                currLink.addClass('animated fadeInUp');
            }  

            if (currLink.offset().top < scrollPos){
                currLink.addClass('animated fadeInUp');
            }          
        });
        
        $('.for_animate2').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos2 && currLink.offset().top + currLink.height() > scrollPos2) {                
                currLink.addClass('animated fadeIn');
            }  

            if (currLink.offset().top < scrollPos2){
                currLink.addClass('animated fadeIn');
            }          
        });

        $('.for_animate3').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos3 && currLink.offset().top + currLink.height() > scrollPos3) {                
                currLink.addClass('animated fadeIn');
            }  

            if (currLink.offset().top < scrollPos3){
                currLink.addClass('animated fadeIn');
            }          
        });

        $('.for_animate4').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos4 && currLink.offset().top + currLink.height() > scrollPos4) {                
                currLink.addClass('animated fadeIn');
            }  

            if (currLink.offset().top < scrollPos4){
                currLink.addClass('animated fadeIn');
            }          
        });

        $('.for_animate_right').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos && currLink.offset().top + currLink.height() > scrollPos) {                
                currLink.addClass('animated slideInRight');
            }  

            if (currLink.offset().top < scrollPos){
                currLink.addClass('animated slideInRight');
            }          
        });

        $('.for_animate_left').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos && currLink.offset().top + currLink.height() > scrollPos) {                
                currLink.addClass('animated slideInLeft');
            }  

            if (currLink.offset().top < scrollPos){
                currLink.addClass('animated slideInLeft');
            }          
        });

        $('.for_animate_left2').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos2 && currLink.offset().top + currLink.height() > scrollPos2) {                
                currLink.addClass('animated slideInLeft');
            }  

            if (currLink.offset().top < scrollPos2){
                currLink.addClass('animated slideInLeft');
            }          
        });

        $('.for_animate_right2').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos2 && currLink.offset().top + currLink.height() > scrollPos2) {                
                currLink.addClass('animated slideInRight');
            }  

            if (currLink.offset().top < scrollPos2){
                currLink.addClass('animated slideInRight');
            }          
        });

        $('.img_blinds').each(function() {
            var currLink = $(this);
            if (currLink.offset().top <= scrollPos && currLink.offset().top + currLink.height() > scrollPos) {                
                currLink.addClass('slice');
            }  

            if (currLink.offset().top < scrollPos){
                currLink.addClass('slice');
            }          
        });
    }

/**/

/*OpenModalWindow*/
    function ModalWindow(){
        var open_modal = $('.open_modal'),
            modal_form = $('.modal_window'),
            close_window = $('.modal_close');

            open_modal.bind('click', function(e){
                var id_wind = $(this).attr('href');
                modal_form.fadeOut(300);
                $(id_wind).fadeIn(300);
                e.preventDefault();
			
            });

            close_window.on('click', function(e){
                modal_form.fadeOut(300);
                e.preventDefault();
            });
    }
	function ModalWindowProducts(){
        var open_modal = $('.open_modal_product'),
            modal_form = $('.modal_window'),
            close_window = $('.modal_close');
            
			
			
            open_modal.bind('click', function(e){
                var id_wind = $(this).attr('href');
				var name= $(this).attr('data-name');
				var color= $(this).attr('data-color');
				var price= $(this).attr('data-price');
				
				modal_form.find('input[name="product"]').val(name+" "+color+" стоимостью "+price);
				modal_form.find('input[name="product-link"]').val(window.location);
                modal_form.fadeOut(300);
                $(id_wind).fadeIn(300);
                e.preventDefault();
				
				
				$('.order_product').click(function(event){
									event.preventDefault();
									 post=$('#productform').serialize();
									 if($('#productform input[name="phone"]').val().length>0){
									 $.ajax({
										url: '/orderproductcreate',
										type: "POST",
										data : post, 
										success: function (data) {
											$('#productform').hide();
											$('.form_container h4').hide();
											$('.call_result').remove();
											$('.form_container').append('<br><p style="text-align:center; font-family: Roboto;font-size: 20px!important; color: #25272a;margin-top:-12px;" class="call_result">Заявка принята. В ближайшее время наш менеджер свяжется с Вами.</p><br>')
											//fbq('track', 'InitiateCheckout');
										

										}
									 });
									 }
								});

                $('.order_call').click(function(event){
                    event.preventDefault();
                    post=$('#order_call_form').serialize();
                    if($('#order_call_form input[name="phone"]').val().length>0){
                        $.ajax({
                            url: '/ordercallcreate',
                            type: "POST",
                            data : post,
                            success: function (data) {
                                $('#order_call_form').hide();
                                $('.form_container h4').hide();
                                $('.call_result').remove();
                                $('.form_container').append('<br><p style="text-align:center; font-family: Roboto;font-size: 20px!important; color: #25272a;margin-top:-12px;" class="call_result">Заявка принята. В ближайшее время наш менеджер свяжется с Вами.</p><br>')
                                //fbq('track', 'InitiateCheckout');


                            }
                        });
                    }
                });
			
            });

            close_window.on('click', function(e){
                modal_form.fadeOut(300);
                e.preventDefault();
            });
    }
	
	function ModalWindowProducts_request(){
        var open_modal = $('.open_modal_product_request'),
            modal_form = $('.modal_window'),
            close_window = $('.modal_close');
            
			
			
            open_modal.bind('click', function(e){
                var id_wind = $(this).attr('href');
				var name= $(this).attr('data-name');
				var color= $(this).attr('data-color');
				var price= $(this).attr('data-price');
				
				modal_form.find('input[name="product"]').val(name+" "+color);
				modal_form.find('input[name="product-link"]').val(window.location);
				
                modal_form.fadeOut(300);
                $(id_wind).fadeIn(300);
                e.preventDefault();
				
				
				$('.order_product').click(function(event){
									event.preventDefault();
									 post=$('#price_request_form').serialize();
									 if($('#price_request_form input[name="phone"]').val().length>0){
									 $.ajax({
										url: '/requestcreate',
										type: "POST",
										data : post, 
										success: function (data) {
											$('#price_request_form').hide();
											$('.form_container h4').hide();
											$('.call_result').remove();
											$('.form_container').append('<br><p style="text-align:center; font-family: Roboto;font-size: 20px!important; color: #25272a;margin-top:-12px;" class="call_result">Запрос отправлен. В ближайшее время наш менеджер свяжется с Вами.</p><br>')
											//fbq('track', 'InitiateCheckout');
										

										}
									 });
									 }
								});
				
				
			
            });

            close_window.on('click', function(e){
                modal_form.fadeOut(300);
                e.preventDefault();
            });
    }

/**/
/*Searform*/
function SearchFormScript() {
    var selectors = {
        sSearchform: '.js--searchform',
        sInput: '.js--search-text',
        sOpenClose: '.js--searchform-open',
        sHeader: '.js--searchform-head',
        sGo: '.js--searchform-go',
        sTabs: '.js--search-tabs',
        sTabsClass: '.js--tabs',
        sBody: '.js--searchform-body',
        catalogBtn: '.js--list-catalog-btn',
        sHiddenClass: 'is--hidden',
        sOpenClass: 'is--open-searchform',
        sActiveClass: 'is--active',
        sScrollClass: 'is--scroll',

        listCatalog: '.js--list-catalog',
        catalogBtn: '.js--list-catalog-btn',
        catalogBlock: '.js--list-catalog-block',
        catalogFon: '.js--list-catalog-fon',
        isOpen: 'is--open',
        isHidden: 'is--hidden'
    }

    function SearchGo() {



        if ($(selectors.sInput).val() != '') {
            searchString=$(selectors.sInput).val();
            $.ajax({
                url: '/searchajax',
                type: "POST",
                dataType: 'json',
                data:{searchString:searchString},
                success: function (data) {
					
                    $('a#atab01').text('Категорий ('+data.categories.length+');');
                    $('a#atab02').text('Брендов ('+data.brands.length+');');
                    $('a#atab03').text('Услуг ('+data.services.length+');');
                    $('a#atab04').text('Товаров ('+data.products.length+');');

                    var categories="";
                    if(data.categories.length>0) {
                        data.categories.forEach(function (item, i, arr) {
                            categories += '<li><a href="' + item['url'] + '" class="tbl"><div class="img" style="background-image: url(' + String(item['img']) + ');"></div><div class="in"><h3>' + item['title'] + '</h3> </div></a></li>';
                        });
                        $('div#stab01 ul.services_list_in').html(categories);
                    };
                    if(data.categories.length>0) {
                        var brands = "";
                        data.brands.forEach(function (item, i, arr) {
                            brands += '<li><a href="' + item['url'] + '" class="tbl"><div class="img" style="background-image: url(' + String(item['img']) + ');"></div><div class="in"><h3>' + item['title'] + '</h3> </div></a></li>';
                        });
                        $('div#stab02 ul.services_list_in').html(brands);
                    };
                    if(data.services.length>0) {
                        var services = "";
                        data.services.forEach(function (item, i, arr) {
                            services += '<li><a href="' + item['url'] + '" class="tbl"><div class="img" style="background-image: url(' + String(item['img']) + ');"></div><div class="in"><h3>' + item['title'] + '</h3> </div></a></li>';
                        });
                        $('div#stab03 ul.services_list_in').html(services);
                    };
                    if(data.products.length>0) {
                        var products = "";
                        data.products.forEach(function (item, i, arr) {
                            products += '<li><a href="' + item['url'] + '" class="tbl"><div class="img" style="background-image: url(' + String(item['img']) + ');"></div><div class="in"><h3>' + item['title'] + '</h3> </div></a></li>';
                        });
                        $('div#stab04 ul.services_list_in').html(products);
                    };
                }
            });


            if (!$(selectors.sBody).hasClass(selectors.sActiveClass)) {
                $(selectors.sBody).addClass(selectors.sActiveClass);
                $(selectors.sBody).fadeIn(300);
                $(selectors.sTabsClass).hide();
                // Первому не пустому табу (выдаче)
                $('#stab01').fadeIn(300);
                $(selectors.sTabs).find('a').removeClass(selectors.sActiveClass);
                $('#atab01').addClass(selectors.sActiveClass);
            }
        } else {
            $(selectors.sBody).removeClass(selectors.sActiveClass);
            $(selectors.sBody).fadeOut(300);
        }
    }

    $(selectors.sOpenClose).on('click', function(el) {
        if ($(selectors.sOpenClose).hasClass(selectors.sActiveClass)) {
            $(selectors.sOpenClose).removeClass(selectors.sActiveClass);
            $(selectors.sSearchform).fadeOut(300);
            $('header').removeClass(selectors.sOpenClass);
            $('body').removeClass(selectors.sHiddenClass);
        } else {
            $(selectors.sOpenClose).addClass(selectors.sActiveClass);
            $(selectors.sSearchform).fadeIn(300);
            $(selectors.sInput).focus();
            $('header').addClass(selectors.sOpenClass);
            $('body').addClass(selectors.sHiddenClass);
        }

        if ($(selectors.listCatalog).hasClass(selectors.isOpen)) {
            $(selectors.listCatalog).removeClass(selectors.isOpen);
            $(selectors.catalogBlock).fadeOut(300);
            $(selectors.catalogFon).fadeOut(300);
            $('body').removeClass(selectors.isHidden);
        }
        el.preventDefault();
    });

    $(selectors.sBody).on('scroll', function() {
        if (!$(selectors.sSearchform).hasClass(selectors.sScrollClass)) {
            $(selectors.sSearchform).addClass(selectors.sScrollClass);
        }
    });

    $(selectors.sTabs).find('a').on('click', function(el) {
        var idTab = $(this).attr('href');
        if (!$(this).hasClass(selectors.sActiveClass)) {
            $(selectors.sTabs).find('a').removeClass(selectors.sActiveClass);
            $(this).addClass(selectors.sActiveClass);
            $(selectors.sTabsClass).hide();
            $(idTab).fadeIn(300);
        }
        el.preventDefault();
    });

    $(selectors.sGo).on('click', function(el) {
        SearchGo();
        el.preventDefault();
    });

    $(selectors.sInput).keyup(function () {
        SearchGo();
    });
}
/*Slide Main Animate Add Class*/
    function MainSlideAnimate() {
        var selectors = {
            sBlock: '.js--mslide',
            sHeader: '.js--mslide-h',
            sText: '.js--mslide-txt',
            sPhone: '.js--mslide-ph',
            sActiveClass: 'is--active'
        }

        function SetTimeoutAnimate(el, tm) {
            setTimeout(function(){
                $(el).addClass(selectors.sActiveClass);
            }, tm);
        }

        SetTimeoutAnimate(selectors.sBlock, 500);
        SetTimeoutAnimate(selectors.sHeader, 750);
        SetTimeoutAnimate(selectors.sText, 1200);
        SetTimeoutAnimate(selectors.sPhone, 1850);
    }
/*HoverComplex*/
    function HoverComplex(){
        var complex_list = $('.complex_list'),
            link = complex_list.find('.in_to'),
            images_fon = $('.images_fon'),
            fon = images_fon.find('div');

            link.mouseover(function(){
                var id_fon = $(this).attr('data-id');
                fon.removeClass('active');
                $(id_fon).addClass('active');                
            }).mouseout(function(){
                fon.removeClass('active');  
            });
    }

/**/

/*Select Color*/
    function SelectColors() {
        var $optionColors = $('.selectpicker .option-colors');
        var $bootstrapOptionColor = $('.select_mini .bootstrap-select .option-colors');
        var colorArr = [];
        var dataColor1 = [];
        var dataColor2 = [];
        var i = 0;
        var j = 0;
        $optionColors.each(function(){
            var th = $(this);
            dataColor1[i] = th.attr('data-color1');
            dataColor2[i] = th.attr('data-color2');
            i++;
        });
        $bootstrapOptionColor.each(function(){
            var th = $(this);
            th.closest('li').find('a').append('<div class="before" style="background-color: '+dataColor1[j]+'"></div>');
            th.closest('li').find('a').append('<div class="after" style="background-color: '+dataColor2[j]+'"></div>');
            console.log(dataColor1[j],dataColor2[j]);
            j++;
        });
    }

/*--- Device ---*/
var isiPad = false;
if (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)) { 
    isiPad = true; 
};
var iPhone = false;
if (navigator.userAgent.match(/iPhone/i)) {
    iPhone = true;
}
function Apple(){
    if (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i)) {

    }
}

//Order Radio
    function OrderRadio(){
        selectors = {
            orList: '.js--radio-list',
            orSubList: '.js--radio-list_sub',
            orListParent: '.js--radio-list-parent',
            orRadio: '.js--bs-radio',
        }

        $(selectors.orRadio).on('ifChecked', function(event){
            var th = $(this);
            if (th.attr('data-type') == 'parent') {
                $(this).closest(selectors.orListParent).find(selectors.orSubList).slideDown(300);
            }
        });

        $(selectors.orRadio).on('ifUnchecked', function(event){
            var th = $(this);
            if (th.attr('data-type') == 'parent') {
                var th_sub = $(this);
                th_sub.closest(selectors.orListParent).find(selectors.orSubList).slideUp(300);
                th_sub.closest(selectors.orListParent).find(selectors.orSubList).find(selectors.orRadio).each(function(){
                    $(this).iCheck('uncheck');
                });
            }
        });
    }

//Color Change
    function ColorChangeBasket() {
        var selectors = {
            bsImageColor: '.js--bs-image-color',
            bsColor: '.js--bs-color',
            bsColorHidden: '.js--bs-color-hidden',
            bsRadioColor: '.js--bs-radio-color',
            bsTr: '.js--bs-tr',
        }

        $(selectors.bsRadioColor).on('ifChecked', function(event){
            var th = $(this),
                prColor = th.attr('data-color'),
                prNumb = th.attr('data-product');

            $(selectors.bsTr).each(function(){
                var th2 = $(this);
                if (th2.attr('data-product') == prNumb) {
                    //change image color
                    var imageColor = $(this).find(selectors.bsImageColor).find('img');
                    imageColor.removeClass('active');
                    imageColor.each(function(){
                        var th3 = $(this);
                        if (th3.attr('data-color') == prColor) {
                            th3.addClass('active');
                        }
                    });

                    //change color
                    $(this).find(selectors.bsColor).css('background-color',prColor);
                    $(this).find(selectors.bsColorHidden).val(prColor);
                }
            });
        });

        /*$(selectors.bsRadioColor).on('ifUnchecked', function(event){
            
        });*/
    }

/*Rozetka*/
/*Select Fon for Product*/
function BgChangeFon() {
    var selectors = {
        item: '.bg-colors-list li',
        result: '.configurator-bg-colors-result',
    }
    $(selectors.item).on('click', function(){
        var bg = $(this).data('color');
        console.log(bg);
        $(selectors.result).css('background', 'url(img/fon/'+bg+')');
        if (!$(this).hasClass('active')) {
            $(selectors.item).removeClass('active');
            $(this).addClass('active');
        }
    });
}


$(window).bind('scroll',function(e){

});

$(window).load(function(){

});

$(window).resize(function(){

});

$(document).ready(function() {
    BgChangeFon();

    /*function ajaxFunction(scriptUrl,parentRes,color,part,material,filter_click) {
        var hidden = $("body").data("hidden");
        if (hidden != 1) $('.preload-'+part).css("display","block");
        $.ajax({
            type: "POST",
            url: scriptUrl,
            dataType: "json",
            cache: false,
            data: {
                parent: parentRes,
                part: part,
                color: color,
                material: material,
                curl: window.location.href,
                hidden: hidden
            },
            success: function(data) {
                if (!filter_click) addPageParametrs(data.url);
                if (hidden != 1) {
                    $('.preload-'+part).css("display","none");
                    $(data.block+" .products-content").empty().append(data.output);
                    if (data.part == "switch") {
                        prodsSplitter();
                        if (!filter_click) {
                            $(".switch-block .product-content").mechFilter(".type-filter");

                            if (data.min_switch == 0) $(".min-price-switch").parents('.min-price-row').addClass('hide');
                            else {
                                $(".min-price-switch").parents('.min-price-row').removeClass('hide');
                                $(".min-price-switch").text(data.min_switch);
                            }
                            if (data.min_socket == 0) $(".min-price-socket").parents('.min-price-row').addClass('hide');
                            else {
                                $(".min-price-socket").parents('.min-price-row').removeClass('hide');
                                $(".min-price-socket").text(data.min_socket);
                            }

                            var count = $(".switch-block .ms2_product").length,
                                ending = wordEnding(count,'','Р°','РѕРІ');
                            $(".switch-count").text(count); $(".switch-ending").text(ending);
                        }
                        else {
                            var checked_filters = '';
                            $(".type-filter li.checked-filter").each(function() {
                                checked_filters += '.switch-block .for-filter-element.'+$(this).data('type')+', ';
                            });
                            checked_filters = checked_filters.replace(/,\s*$/, "");
                            if (checked_filters && $(checked_filters).length) {
                                $(".switch-block .for-filter-element").addClass('hide');
                                $(checked_filters).removeClass('hide');
                            }
                            else {
                                $(".switch-block .product-content").mechFilter(".type-filter");
                            }
                        }
                    }
                    else if (data.part == "frame") {
                        if (!filter_click) {
                            if (data.min_frame == 0) $(".min-price-frame").parents('.min-price-row').addClass('hide');
                            else {
                                $(".min-price-frame").parents('.min-price-row').removeClass('hide');
                                $(".min-price-frame").text(data.min_frame);
                            }
                            var count = $(".frame-block .ms2_product").length,
                                ending = wordEnding(count,'','Р°','РѕРІ');
                            $(".frame-count").text(count); $(".frame-ending").text(ending);
                        }
                    }
                    if ($(".format-list").is(".active")) $(".product-content").addClass("product-list");
                    if (!filter_click) $(".current-"+part+"-color").text(color+" ("+material+")");

                    filterFixed('.switch-sidebar', 'switch');
                    filterFixed('.frame-sidebar', 'frame');
                }
                $(".config-preload").css("display","none");
            }
        });
    }*/

    /*$('.part-color-link').live('click', function () {
        var curr_elem = $(this);
        if (!curr_elem.hasClass('active')) {
            var parent_list = curr_elem.parents(".configurator-colors-list"),
                checkFirst = $(".part-color-link.active", parent_list).is("*");

            if (checkFirst) $(".config-preload").css("display","block");

            var part = curr_elem.data('part'),
                img = curr_elem.data('image'),
                color = curr_elem.data('color'),
                material = curr_elem.data('material'),
                for_kit = curr_elem.data('for-kit'),
                parentRes = curr_elem.data('parent');

            $(".part-color-link", parent_list).removeClass('active');
            $('.kit-frame-color-preview').removeClass('active');
            curr_elem.addClass('active');

            $('.products-sidebar.'+part+'-sidebar .color-filter li').removeClass('checked-filter');
            $('.products-sidebar.'+part+'-sidebar .color-filter li[data-for-kit="'+for_kit+'"]').addClass('checked-filter');

            $('.configurator-'+part+'-result').css('background-image', 'url('+img+')');

            if (checkFirst) ajaxFunction("/assets/ajax/config.php",parentRes,color,part,material);
        }
        return false;
    });*/
    // frames configurator
    var framesSwiper = new Swiper('.frames-colors-wrapper', {
        nextButton: '.frames-button-next',
        prevButton: '.frames-button-prev',
        speed: 400,
        slidesPerView: 'auto',
        spaceBetween: 0,
        wrapperClass: 'frames-colors-list',
        slideClass: 'part-color-link'
    });
    // switch configurator
    var switchSwiper = new Swiper('.switch-colors-wrapper', {
        nextButton: '.switch-button-next',
        prevButton: '.switch-button-prev',
        speed: 400,
        slidesPerView: 'auto',
        spaceBetween: 0,
        wrapperClass: 'switch-colors-list',
        slideClass: 'part-color-link'
    });

    $('.totop').click(function () {
        var elementClick = $(this).attr("href");
        $('html:not(:animated),body:not(:animated)').animate({scrollTop: 0}, 800);
        return false;
    });
   
    /*Open Close Window*/
    $('.open_window').on('click', function(e){
        var id_window = $(this).attr('data-id-window');
        $(id_window).fadeIn(200);        
        $('.fon_window').fadeIn(180);
        $('html:not(:animated),body:not(:animated)').animate({scrollTop: 0}, 800);
        return false;
        e.preventDefault();
    });

    $('.modal_close').on('click', function(e){
        $('.modal-form').fadeOut(200);
        $('.fon_window').fadeOut(180);
        e.preventDefault();
    });
    $('.fon_window').on('click', function(){
        $('.modal-form').fadeOut(200);
        $('.fon_window').fadeOut(180);
    });
    
    $('.disable').on('click', function(e){
        e.preventDefault();
    });

    if($('.check').length != ''){
        $('.check').iCheck({
            checkboxClass: 'icheckbox_minimal-aero',
            increaseArea: '20%'
          });
    }

    if($('.radio-type').length != ''){
        $('.radio-type').iCheck({
            radioClass: 'iradio_minimal-aero',
            increaseArea: '20%'
          });
    }

	$('.cat_selector li a').click(function(){
		
		window.location.href = $(this).attr('href');
		
		
	});
	$('.catalog_cat_selector li a').click(function(){
		
		var cat_url=$(this).attr('href');
		$('form.filter_form').attr('action',cat_url);
		
	});
     $('select.cat_select_top').change(function(){
		 window.location.href =$(this).val();
	 });
	   
	$('#filter_form_cat a.send_filter').click(function(event){
		event.preventDefault();
		$('#filter_form_cat').submit();	
	});   
        

     /*$('.contactsform .button').click(function(e){
		  e.preventDefault();
		  post=$('.contactsform').serialize();
		  if($('.contactsform input[name="phone"]').val().length>0){
			 $.ajax({
				url: '/contactformmessagecreate',
				type: "POST",
				data : post, 
				success: function (data) {
					$('.contactsform').hide();
					$('.container_mobile h4').hide();
					$('.call_result').remove();
					$('.forms_block').append('<br><p style="text-align:center; font-family: Roboto;font-size: 20px!important; color: #25272a;margin-top:-12px;" class="call_result">Запрос отправлен. В ближайшее время наш менеджер свяжется с Вами.</p><br>')
					//fbq('track', 'InitiateCheckout');

				}
		  });
		  }
	 });*/

    if($('.js--bs-radio').length != ''){
        OrderRadio();
    }
    if($('.js--bs-radio-color').length != ''){
        ColorChangeBasket();
    }

    
});